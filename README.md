# Ocean Navigation

Ocean Navigation is divided into two sections:
- Calculations in **VoyagePlan**,
- Calculations in **Celestial Navigation**.

Under each section are several different tasks.

You can find different Python programs solving upper calculation problems in this repository. For the Navigation Almanac, we use [Skyfield](https://anaconda.org/conda-forge/skyfield) module. 

## Voyage Plan

In navigation business, Voyage Plan deals primarily with three tasks:

1. How do we design the **Mercator Chart**?
2. How do we compute the **Rumb Line** (RL) or **Loxodrome** problem?
3. How do we compute the **Great Circle** (GC) or **Orthodrome** problem?

## Celestial Navigation

In Celestial Navigation are a few essential tasks:

1. How do we identify stars or **Star Identification** problem?
2. How do we compute the **Sight Reduction** problem or the **Intercept Method** problem?
3. How do we compute the **Meridian Passage** problem?
4. How do we compute **Rise** and **Set** of celestial bodies?
5. How do we compute **Compass Deviation** or compass correction using celestial bodies?
6. How do we draw or compute our position using celestial bodies?
